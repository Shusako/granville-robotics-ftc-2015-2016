package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

public class MPTeleOp extends OpMode {

    DcMotor motorRightFront;
    DcMotor motorLeftFront;
    DcMotor motorRightRear;
    DcMotor motorLeftRear;
    DcMotor motorArm;
    DcMotor motorPickup;
    Servo servoDump;

    /**
     * Constructor
     */
    public static void TestOp() {

    }

    /*
     * Code to run when the op mode is first enabled goes here
     *
     * @see com.qualcomm.robotcore.eventloop.opmode.OpMode#start()
     */
    @Override
    public void init() {
        motorRightFront = hardwareMap.dcMotor.get("rightF_motor");
        motorRightRear = hardwareMap.dcMotor.get("rightR_motor");
        motorLeftFront = hardwareMap.dcMotor.get("leftF_motor");
        motorLeftRear = hardwareMap.dcMotor.get("leftR_motor");
        servoDump = hardwareMap.servo.get("dump");

        motorRightFront.setDirection(DcMotor.Direction.REVERSE);
        motorRightRear.setDirection(DcMotor.Direction.REVERSE);

        motorArm = hardwareMap.dcMotor.get("arm_motor");

        motorArm.setMode(DcMotorController.RunMode.RUN_USING_ENCODERS);

        motorPickup = hardwareMap.dcMotor.get("pickup_motor");
        motorPickup.setDirection(DcMotor.Direction.REVERSE);
    }

    public float maxDeltaPower = 0.02F; //max change in power allowed per cycle adjust as needed
    public float lastLeftPower, lastRightPower;
    public long  lastTimeDeltaPower;

    public static final float JOY_THRESH = 0.05F;  // If stick value is less than threshold make it zero
    public static final float ARM_UP_POWER = -0.3F;  // POWER FOR RAISING ARM       NEGATIVE IS UPWARDS
    public static final float ARM_DOWN_POWER = 0.03F;  // POWER FOR LOWERING ARM    POSITIVE IS DOWNWARDS
    public static final float ARM_UNFOLD_POWER = 0.2F;  // POWER FOR UNFOLDING ARM  POSITIVE IS DOWNWARDS

    /*
     * This method will be called repeatedly in a loop
     *
     * @see com.qualcomm.robotcore.eventloop.opmode.OpMode#run()
     */
    @Override
    public void loop() {
        /** NOTES
        /* Encoders
         * motorArm.getCurrentPosition();//
         * motorArm.setTargetPosition(position);//
         * motorArm.setMode(DcMotorController.RunMode.RESET_ENCODERS);
         * motorArm.setMode(DcMotorController.RunMode.RUN_USING_ENCODERS);
         * motorArm.setMode(DcMotorController.RunMode.RUN_TO_POSITION); // power is always positive
         * motorArm.setMode(DcMotorController.RunMode.RUN_WITHOUT_ENCODERS); // default
        */

        float power     = -gamepad1.right_stick_y;
        float leftStick =  gamepad1. left_stick_x;

        if(Math.abs( power     ) <= JOY_THRESH) power     = 0;
        if(Math.abs( leftStick ) <= JOY_THRESH) leftStick = 0;

        float left  = power + leftStick;
        float right = power - leftStick;

        // restrict the right/left values so that the values never exceed +/- 1
        left  = Range.clip(left, -1, 1);
        right = Range.clip(right, -1, 1);

        // these two condition statements control the change of power
        if((Math.abs(left  -  lastLeftPower) > maxDeltaPower)) {     // If the change of power is greater than maxDeltaPower
            int direction = ( left -  lastLeftPower) < 0 ? -1 : 1; // positive is forward, negative is backwards
            left   =  lastLeftPower + (maxDeltaPower * direction); // set variable left (used as motor_left power), this is the part that actually calculates what the power should be to comply with maxDeltaPower
        }
        if((Math.abs(right - lastRightPower) > maxDeltaPower)) {   // If the change of power is greater than maxDeltaPower
            int direction = (right - lastRightPower) < 0 ? -1 : 1; // positive is forward, negative is backwards
            right  = lastRightPower + (maxDeltaPower * direction); // set variable right (used as motor_right power), this is the part that actually calculates what the power should be to comply with maxDeltaPower
        }

        if(lastTimeDeltaPower < System.currentTimeMillis()) { // if a certain amount of time has passed
            if     ( gamepad1.left_bumper) maxDeltaPower -= 0.001F; //  if left bumper is being pressed, make max delta power less
            if     (gamepad1.right_bumper) maxDeltaPower += 0.001F; // if right bumper is being pressed, make max delta power more
            maxDeltaPower   = Range.clip(maxDeltaPower, 0, 0.075F); // lock max delta power to between 0 and 0.075
            lastTimeDeltaPower = System.currentTimeMillis() + 150L; // set next time to check button positions to be in 150 milliseocnds
        }

        // write the values to the motors
        motorLeftFront .setPower( left);
        motorRightFront.setPower(right);
        motorLeftRear  .setPower( left);
        motorRightRear .setPower(right);
        lastLeftPower  =  left;
        lastRightPower = right;


        float armPower = 0;

             if (gamepad1.b) armPower = ARM_DOWN_POWER  ; // move arm downwards
        else if (gamepad1.x) armPower = ARM_UP_POWER    ; // move arm upwards
        else if (gamepad1.y) armPower = ARM_UNFOLD_POWER; // move arm upwards

        if(Math.abs(gamepad2.left_stick_y) >= JOY_THRESH) {
            armPower = -gamepad2.left_stick_y;
            if(armPower > 0) armPower /= 30;
            else armPower /= 3;
        }

        motorArm.setPower(armPower);

        if(gamepad2.dpad_down  ) motorPickup.setPower(0.25);
        if(gamepad2.dpad_left  ) motorPickup.setPower(0.50);
        if(gamepad2.dpad_right ) motorPickup.setPower(0.75);
        if(gamepad2.dpad_up    ) motorPickup.setPower(1.00);

        if(gamepad2.y) motorPickup.setPower(0);

        if(gamepad2.a) servoDump.setPosition(0);
        if(gamepad2.b) servoDump.setPosition(1);

        telemetry.addData("Text", "*** Robot Data***");
        telemetry.addData("left tgt pwr",  "left  pwr: " + String.format("%.2f", left));
        telemetry.addData("right tgt pwr", "right pwr: " + String.format("%.2f", right));
        telemetry.addData("dp", String.format("%.3f", maxDeltaPower));
        telemetry.addData("arm1", motorArm.getCurrentPosition());
        telemetry.addData("arm2", motorArm.getCurrentPosition());
    }

    /*
     * Code to run when the op mode is first disabled goes here
     *
     * @see com.qualcomm.robotcore.eventloop.opmode.OpMode#stop()
     */
    @Override
    public void stop() {

    }
}
