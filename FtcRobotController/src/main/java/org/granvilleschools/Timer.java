package org.granvilleschools;

/**
 * Created by FRC4283 on 12/2/2015.
 */
public class Timer {

    public long interval;
    public long lastTime;

    public Timer(long l) {
        this.setInterval(l);
    }

    public boolean hasPassed() {
        return this.lastTime < System.currentTimeMillis();
    }

    public void use() {
        this.lastTime = System.currentTimeMillis() + interval;
    }

    public void setInterval(long l) {
        this.interval = l;
    }
}
